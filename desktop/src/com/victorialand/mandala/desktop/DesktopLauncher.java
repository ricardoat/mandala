package com.victorialand.mandala.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.victorialand.mandala.Main;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "mandala";
		config.useGL30 = true;
		config.height = 512;
		config.width = 288;

		new LwjglApplication(new Main(), config);
	}
}
