package com.victorialand.mandala;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.victorialand.mandala.vlf.configuration.Config;
import com.victorialand.mandala.vlf.utils.ImageRect;
import com.victorialand.mandala.vlf.utils.VL;


/**
 * Created by Santiago on 02/08/2016.
 */
public class LoadingScreen extends ScreenAdapter {

    SpriteBatch batch;
    ImageRect splash;
    float time;

    @Override
    public void show(){
        batch = new SpriteBatch();

        Assets.splashImage = new Texture(Gdx.files.internal("data/splash.png"));
        Assets.verdana              = new FreeTypeFontGenerator(Gdx.files.internal("fonts/verdana.ttf"));

        Assets.imgCircles = new Texture[] {
                new Texture(Gdx.files.internal("1.png")),
                new Texture(Gdx.files.internal("2.png")),
                new Texture(Gdx.files.internal("3.png")),
                new Texture(Gdx.files.internal("4.png")),
                new Texture(Gdx.files.internal("5.png")),
                new Texture(Gdx.files.internal("6.png")),
                new Texture(Gdx.files.internal("7.png"))
        };

        VL.fontParameter.size       = Math.round(30 * Config.ssm.referenceWidthFactor);
        Main.debugFont              = Assets.verdana.generateFont(VL.fontParameter);

        splash = new ImageRect(Assets.splashImage,0,0,Config.ssm.width, Config.ssm.height, "splash");
        splash.setX(Config.ssm.getFixedBackgroundX());
    }

    @Override
    public void render(float deltaTime){

        time += deltaTime;
        if (time>0.5f){
            Config.game.setScreen(new GameScreen());
        }

        batch.begin();
        splash.draw(batch, 1);
        batch.end();

    }


    @Override
    public void dispose(){
        batch.dispose();
    }
}
