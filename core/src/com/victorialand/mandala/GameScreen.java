package com.victorialand.mandala;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.victorialand.mandala.game.Mandala;
import com.victorialand.mandala.vlf.configuration.Config;
import com.victorialand.mandala.vlf.utils.Debugger;


/**
 * Created by Santiago on 18/09/2016.
 */

public class GameScreen extends ScreenAdapter implements InputProcessor{

    public static int scrollY;
    SpriteBatch batch;
    Stage stage;
    Mandala mandala;
    int rotationTime = 2;
    int rotationDegree = 45;

    @Override
    public void show () {

        Gdx.input.setInputProcessor(this);
        Main.debugger = new Debugger();
        Main.debugger.setColor(Color.RED);

        batch = new SpriteBatch();
        stage = new Stage();

        mandala = new Mandala(Config.ssm.width/2,Config.ssm.height/2);
        stage.addActor(mandala);
        stage.addActor(Main.debugger);

    }

    public void update(float deltaTime){
        stage.act();

    }

    @Override
    public void render (float deltaTime) {
        update(deltaTime);
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        stage.draw();
        batch.end();

    }


    @Override
    public void dispose () {
        batch.dispose();
        stage.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE){
                Gdx.app.exit();
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        mandala.rotate(rotationTime,rotationDegree,screenX,screenY);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
