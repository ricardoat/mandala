package com.victorialand.mandala;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.victorialand.mandala.vlf.FirstScreen;
import com.victorialand.mandala.vlf.configuration.Config;
import com.victorialand.mandala.vlf.configuration.Orientation;
import com.victorialand.mandala.vlf.configuration.Positions;
import com.victorialand.mandala.vlf.configuration.ScreenSizeManager;
import com.victorialand.mandala.vlf.configuration.SizeBehavior;
import com.victorialand.mandala.vlf.configuration.Sizes;
import com.victorialand.mandala.vlf.utils.Debugger;
import com.victorialand.mandala.vlf.utils.Logger;


import java.util.Random;

public class Main extends Game {

	public static BitmapFont debugFont, FMFont;
	public static Debugger debugger;
	public static Preferences preferences;
	public static Random randomGenerator;

	@Override
	public void create() {
		Config.game = this;
		Config.ssm = new ScreenSizeManager(SizeBehavior.STRETCH, Orientation.PORTRAIT);
		Config.sizes = new Sizes();
		Config.positions = new Positions();
		randomGenerator = new Random();
		preferences = Gdx.app.getPreferences("generic");

		setScreen(new FirstScreen());
	}
}
