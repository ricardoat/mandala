package com.victorialand.mandala.vlf;

import com.victorialand.mandala.vlf.configuration.Config;

/**
 * Created by Santiago on 19/09/2016.
 */
public class GameConfig {

    public static String externalPath = "/home/ricardo/Documents/Docs%20Ubuntu/victorialand/mandala";
    public static boolean readExternalInput = false;
    public static boolean writeExternalOutput = false;
    public static boolean log = false;
    public static float planetSize = Config.ssm.width*0.5f;

    //
    // acá ponés cualquier variable de configuración que se te cante.
    //

}
