package com.victorialand.mandala.vlf.configuration;

import com.badlogic.gdx.Gdx;
import com.victorialand.mandala.vlf.GameConfig;

/**
 * Created by Santiago on 26/07/2016.
 */


public class ScreenSizeManager {

    private int referenceWidth = 720, referenceHeight = 1280;
    private int baseWidth = 240, baseHeight = 320;
    private int extremeWidth = 240, extremeHeight = 432;
    public int extremeLongestDimension;
    public int baseLongestDimension;
    public int baseExtremeDifference;
    public int baseCurrentDifference;
    public String sizeDir;
    public Orientation orientation;
    private float baseAspectRatio;
    private float extremeAspectRatio;
    private float stretchingFactor;
    private SizeBehavior behavior;

    public int width;
    public int height;
    public float referenceWidthFactor;
    public float referenceHeightFactor;

    public ScreenSizeManager(SizeBehavior behavior, Orientation orientation){

        width                   = Gdx.graphics.getWidth();
        height                  = Gdx.graphics.getHeight();
        referenceWidthFactor    = (float) width / referenceWidth;
        referenceHeightFactor   = (float) height / referenceHeight;

        this.orientation        = orientation;
        this.behavior           = behavior;

        int screenSize = height;
        if (orientation == Orientation.PORTRAIT){
            screenSize = width;
        }

        if (screenSize <= 320){
            sizeDir = "low/";
        }
        else if (screenSize <= 480){
            sizeDir = "medium/";
        }
        else if (screenSize <= 600){
            sizeDir = "high/";
        }
        else{
            sizeDir = "xhigh/";
        }

        int currentLongestDimension;
        if(orientation == Orientation.LANDSCAPE){
            baseAspectRatio         = (float) baseWidth / baseHeight;
            extremeAspectRatio      = (float) extremeWidth / extremeHeight;
            currentLongestDimension = width;
            baseLongestDimension    = (int) (height * baseAspectRatio);
            extremeLongestDimension = (int) (height * extremeAspectRatio);

            stretchingFactor = width / height;
        }
        else{
            baseAspectRatio         = (float) baseHeight / baseWidth;
            extremeAspectRatio      = (float) extremeHeight / extremeWidth;
            currentLongestDimension = height;
            baseLongestDimension    = (int) (width * baseAspectRatio);
            extremeLongestDimension = (int) (width * extremeAspectRatio);

            stretchingFactor = height / width;
        }
        baseExtremeDifference = (int)(extremeLongestDimension - baseLongestDimension) / 2;
        baseCurrentDifference = (int)(currentLongestDimension - baseLongestDimension) / 2;

    }

    public int getHeight(float factor, float ratio){

        switch (orientation){
            case LANDSCAPE:
                if (behavior == SizeBehavior.ALIGN_CENTER){
                    return (int)(factor * height * ratio);
                }
                else if (behavior == SizeBehavior.STRETCH){
                    return (int)(factor * height * ratio * stretchingFactor);
                }
            case PORTRAIT:
                if (behavior == SizeBehavior.ALIGN_CENTER){
                    return (int)(factor * width * ratio);
                }
                else if (behavior == SizeBehavior.STRETCH){
                    return (int)(factor * width * ratio * stretchingFactor);
                }
        }
        return 0;
    }

    public int getWidth(float factor, float ratio){

        switch (orientation) {
            case LANDSCAPE:
                return (int) (factor * height);
            case PORTRAIT:
                return (int) (factor * width);
        }
        return 0;
    }

    public int getX(float xFactor){
        if (orientation == Orientation.PORTRAIT || behavior == SizeBehavior.STRETCH){
            return (int) (xFactor * width);
        }
        else{
            if (behavior == SizeBehavior.ALIGN_CENTER){
                return (int) (xFactor * baseLongestDimension + baseCurrentDifference);
            }
            else if (behavior == SizeBehavior.STRETCH){
                return (int) (xFactor * baseLongestDimension);
            }
        }
        return 0;
    }

    public int getY(float yFactor){
        if (orientation == Orientation.LANDSCAPE || behavior == SizeBehavior.STRETCH){
            return (int) (yFactor * height);
        }
        else{
            if (behavior == SizeBehavior.ALIGN_CENTER){
                return (int) (yFactor * baseLongestDimension + baseCurrentDifference);
            }
            else if (behavior == SizeBehavior.STRETCH){
                return (int) (yFactor * baseLongestDimension);
            }
        }
        return 0;
    }

    public int getFixedBackgroundX(){
        if (behavior == SizeBehavior.ALIGN_CENTER){
            return -baseExtremeDifference + baseCurrentDifference;
        }
        return 0;
    }

    public void behave(SizeBehavior behavior){
        this.behavior = behavior;
    }


}
