package com.victorialand.mandala.vlf.configuration;


/**
 * Created by Santiago on 26/07/2016.
 */
public class Sizes {

    public Size[] circlesSize = new Size[] {
            new Size(0.07f,1f),
            new Size(0.24f,1f),
            new Size(0.37f,1f),
            new Size(0.58f,1f),
            new Size(0.71f,1f),
            new Size(0.82f,1f),
            new Size(1f,1f)
    };

}
