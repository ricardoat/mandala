package com.victorialand.mandala.vlf.configuration;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by root on 14/05/17.
 */

public class LayerManager extends Actor {

    int scrollX, scrollY;

    public int shiftLeft;
    float shiftRatio, deltaShift, shiftSeconds;
    int totalShift, shiftTarget, shiftSign = -1;


    public LayerManager(float shiftRatio){

        this.shiftRatio  = shiftRatio;

    }

    public void startShift(int shift, int seconds, int shiftSign){

        this.shiftSign = shiftSign;
        startShift(shift,seconds);

    }

    public void startShift(int shift, float seconds){

        totalShift = shiftLeft = Math.round(shift * shiftRatio);
        shiftTarget = (int)(getX() + totalShift)*shiftSign;
        deltaShift = 0;
        shiftSeconds = seconds;

    }

    public void setScrollX(int scrollX){
        this.scrollX = scrollX;
    }

    public int getScrollX(){
        return scrollX;
    }


    @Override
    public void act(float deltaTime){

        if (shiftLeft > 0){
            deltaShift = (deltaTime / shiftSeconds) * totalShift + deltaShift;
            int deltaShiftPx = (int)(deltaShift / 1);
            deltaShift = deltaShift % 1;
            shiftLeft -= deltaShiftPx;

            scrollX+=deltaShiftPx*shiftSign;
        }
        /*else if (scrollX != shiftTarget){
            scrollX = shiftTarget;
        }*/

    }


}
