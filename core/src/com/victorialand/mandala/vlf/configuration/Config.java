package com.victorialand.mandala.vlf.configuration;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.victorialand.mandala.Main;
import com.victorialand.mandala.vlf.utils.ButtonStyle;

/**
 * Created by Santiago on 26/07/2016.
 */
public class Config {

    public static Main game;
    public static ScreenSizeManager ssm;
    public static Sizes sizes;
    public static Positions positions;
    public static ButtonStyle style;


}
