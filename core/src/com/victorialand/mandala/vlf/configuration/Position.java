package com.victorialand.mandala.vlf.configuration;

/**
 * Created by Santiago on 28/07/2016.
 */
public class Position {

    float xFactor;
    float yFactor;
    public int x;
    public int y;

    public Position(float xFactor, float yFactor){
        this.xFactor = xFactor;
        this.yFactor = yFactor;
        x = Config.ssm.getX(xFactor);
        y = Config.ssm.getY(yFactor);
    }


}
