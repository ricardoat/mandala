package com.victorialand.mandala.vlf.configuration;

/**
 * Created by Santiago on 26/07/2016.
 */
public class Size {

    float factor;
    float ratio;
    public int width;
    public int height;

    public Size(float factor, float ratio) {
        this(factor,ratio,true);
    }

    // the argument isBase refers to whether i want the value to be calculated based on the
    // actual current device screen aspect ratio or in the baseAspectRatio value defined in the ssm class
    // which is the aspect ratio of the most "square" screen possible.

    public Size(float factor, float ratio, boolean isBase){
        this.factor = factor;
        this.ratio  = ratio;
        width       = Config.ssm.getWidth(factor, ratio);
        height      = Config.ssm.getHeight(factor, ratio);
    }


}
