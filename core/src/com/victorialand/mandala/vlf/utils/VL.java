package com.victorialand.mandala.vlf.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

/**
 * Created by Santiago on 22/08/2016.
 */
public class VL {

    public static ButtonStyle buttonStyle;
    public static LabelStyle labelStyle;
    public static FreeTypeFontParameter fontParameter = new FreeTypeFontParameter();

    public static Button NewButton(){
        buttonStyle = new ButtonStyle(null,null);
        return new Button(buttonStyle.textButtonStyle);
    }

    public static Button NewButton(Texture textureUp, Texture textureDown){
        if(textureUp == null){
            buttonStyle = new ButtonStyle(null,new BitmapFont());
        }
        else{
            buttonStyle = new ButtonStyle(new SpriteDrawable(new Sprite(textureUp)),new BitmapFont());
        }
        if (textureDown != null){
            buttonStyle.setDrawableDown(new SpriteDrawable(new Sprite(textureDown)));
        }
        return new Button(buttonStyle.textButtonStyle);
    }

    public static TextButton NewButton(Texture textureUp, Texture textureDown, String text, BitmapFont font, Color color){

        buttonStyle = new ButtonStyle(new SpriteDrawable(new Sprite(textureUp)),font);
        buttonStyle.setDrawableDown(new SpriteDrawable(new Sprite(textureDown)));
        buttonStyle.setFontColor(color);
        return new TextButton(text,buttonStyle.textButtonStyle);

    }

    public static Label NewLabel(String text,BitmapFont font, Color color){

        labelStyle = new LabelStyle(font,color);
        return new Label(text,labelStyle);

    }

    public static Label NewLabel(String text,BitmapFont font, Color color, Sprite bgSprite){

        labelStyle = new LabelStyle(font,color);
        labelStyle.background = new SpriteDrawable(bgSprite);
        return new Label(text,labelStyle);

    }



}
