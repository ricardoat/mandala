package com.victorialand.mandala.vlf.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.victorialand.mandala.GameScreen;

import java.util.ArrayList;

/**
 * Created by Santiago on 09/08/2016.
 */
public class Collider {

    private float centerX, centerY;
    private float width, height;
    private ShapeRenderer shapeRenderer;

    public Collider(float width, float height){

        this.width = width;
        this.height = height;
        shapeRenderer = new ShapeRenderer();

    }

    public void setPos(float x, float y){
        this.centerX = x;
        this.centerY = y;
    }

    public void setX(int x){
        this.centerX = x;
    }

    public void setX(float x){
        this.centerX = x;
    }

    public void setY(float y){
        this.centerY = y;
    }

    public float getX(){
        return centerX;
    }

    public float getY(){
        return centerY;
    }

    public float getWidth(){
        return width;
    }

    public float getHeight(){
        return height;
    }

    public float getLeftLimit(){
        return centerX-width/2;
    }

    public float getRightLimit(){
        return centerX + width/2;
    }

    public float getTopLimit(){
        return centerY + height/2;
    }

    public float getBottomLimit(){
        return centerY-height/2;
    }

    public void setSize(float width, float height){
        this.width = width;
        this.height = height;
    }

    public void draw(Color color){

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(color);
        shapeRenderer.line(getLeftLimit(),getBottomLimit()-GameScreen.scrollY,getLeftLimit(),getTopLimit()-GameScreen.scrollY);
        shapeRenderer.line(getRightLimit(),getBottomLimit()-GameScreen.scrollY,getRightLimit(),getTopLimit()-GameScreen.scrollY);
        shapeRenderer.line(getLeftLimit(),getBottomLimit()-GameScreen.scrollY,getRightLimit(),getBottomLimit()-GameScreen.scrollY);
        shapeRenderer.line(getLeftLimit(),getTopLimit()-GameScreen.scrollY,getRightLimit(),getTopLimit()-GameScreen.scrollY);
        shapeRenderer.end();

    }

    public boolean isColliding(Collider collider){
        if (getLeftLimit() < collider.getRightLimit()
            && getRightLimit() > collider.getLeftLimit()
            && getTopLimit() > collider.getBottomLimit()
            && getBottomLimit() < collider.getTopLimit()){
            return true;
        }
        return false;
    }

    public static boolean isCollision(Collider colliders1[], Collider colliders2[]){
        for (int i = 0; i < colliders1.length; i++){
            for(int j = 0; j < colliders2.length; j++){
                if (colliders1[i].getLeftLimit() < colliders2[j].getRightLimit()
                        && colliders1[i].getRightLimit() > colliders2[j].getLeftLimit()
                        && colliders1[i].getTopLimit() > colliders2[j].getBottomLimit()
                        && colliders1[i].getBottomLimit() < colliders2[j].getTopLimit()){
                    return true;
                }

            }
        }
        return false;
    }
    public static Collision getFirstCollision(Collider colliders1[], Collider colliders2[]){
        for (int i = 0; i < colliders1.length; i++){
            for(int j = 0; j < colliders2.length; j++){
                if (colliders1[i].getLeftLimit() < colliders2[j].getRightLimit()
                        && colliders1[i].getRightLimit() > colliders2[j].getLeftLimit()
                        && colliders1[i].getTopLimit() > colliders2[j].getBottomLimit()
                        && colliders1[i].getBottomLimit() < colliders2[j].getTopLimit()){
                    return new Collision(i,j,colliders1[i],colliders2[j]);
                }

            }
        }
        return new Collision();
    }
    public static ArrayList<Collision> getAllCollisions(Collider colliders1[], Collider colliders2[]){
        ArrayList<Collision> collisions = new ArrayList<Collision>();
        for (int i = 0; i < colliders1.length; i++){
            for(int j = 0; j < colliders2.length; j++){
                if (colliders1[i].getLeftLimit() < colliders2[j].getRightLimit()
                        && colliders1[i].getRightLimit() > colliders2[j].getLeftLimit()
                        && colliders1[i].getTopLimit() > colliders2[j].getBottomLimit()
                        && colliders1[i].getBottomLimit() < colliders2[j].getTopLimit()){
                    collisions.add(new Collision(i,j,colliders1[i],colliders2[j]));
                }
            }
        }
        return collisions;
    }


}
