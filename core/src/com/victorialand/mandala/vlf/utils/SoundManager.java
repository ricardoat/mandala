package com.victorialand.mandala.vlf.utils;

import com.badlogic.gdx.audio.Sound;

/**
 * Created by root on 23/06/17.
 */

public class SoundManager {

    public static boolean soundsOn = true;
    public static boolean musicOn = true;
    private static float soundPlaying;
    private static Sound playingSound;

    public static void act(float deltaTime){

        if (soundPlaying > 0f){
            soundPlaying -= deltaTime;
            if(soundPlaying <= 0){
                soundPlaying = 0;
                playingSound = null;
            }
        }

    }

    public static void playSound(Sound sound){

        playSound(sound,1f,0f);

    }

    public static void playSound(Sound sound, float volume, float exclusivityTime){

        if (soundsOn && (soundPlaying == 0 || playingSound != sound) ){
            sound.play(volume);
            if (exclusivityTime > 0){
                playingSound = sound;
                soundPlaying = exclusivityTime;
            }
        }

    }

}
