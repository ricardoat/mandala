package com.victorialand.mandala.vlf.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.victorialand.mandala.Main;
import com.victorialand.mandala.vlf.configuration.Config;

import java.util.ArrayList;

/**
 * Created by Santiago on 27/07/2016.
 */
public class Debugger extends Actor{

    ArrayList<String> names;
    ArrayList<String> values;
    BitmapFont font;
    float initX, initY;
    public int uniqID;

    public Debugger(){

        setZIndex(4);
        values = new ArrayList<String>();
        names   = new ArrayList<String>();
        font    = Main.debugFont;
        initX   = Gdx.graphics.getWidth()*0.1f;
        initY   = Gdx.graphics.getHeight()*0.9f;

        font.setColor(Color.BLACK);
    }

    public int set (String fieldName){
        return set(fieldName,"");
    }

    public int set (String fieldName, float value){
        uniqID++;
        return set(fieldName, Float.toString(value));
    }

    public int set (String fieldName, boolean value){
        if (value){
            return set(fieldName, "true");
        }
        else{
            return set(fieldName, "false");
        }
    }

    public int set(String fieldName, String value){


        if (names.contains(fieldName)){
            values.set(names.indexOf(fieldName), value);
            return names.indexOf(fieldName);
        }
        else{
            names.add(fieldName);
            values.add(value);
            return values.size()-1;
        }

    }

    public void setOnce(String fieldName, String value){

        if (!names.contains(fieldName)){
            set(fieldName,value);
        }

    }

    public void remove(String fieldName){

        if (names.contains(fieldName)){
            names.remove(names.indexOf(fieldName));
            values.remove(names.indexOf(fieldName));
        }

    }

    public void setColor(Color color){
        font.setColor(color);
    }

    @Override
    public void draw(Batch batch, float alpha){

        for(int i = 0; i < values.size(); i++){
            font.draw(batch, names.get(i) + ": " + values.get(i), initX, initY - (30f*i* Config.ssm.referenceHeightFactor));
        }


    }

    public String next(){
        return Integer.toString(names.size());
    }
}
