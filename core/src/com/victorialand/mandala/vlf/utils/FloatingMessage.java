package com.victorialand.mandala.vlf.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.victorialand.mandala.Main;

/**
 * Created by Santiago on 09/08/2016.
 */
public class FloatingMessage extends Actor{

    BitmapFont font;
    String text;
    Color color;
    float lifeTime;
    float time;
    float speed = 2;

    public FloatingMessage(float time, String text, int x, int y){

        this.time = lifeTime = time;
        this.text   = text;
        font = Main.FMFont;
        setPosition(x,y);
        color = new Color(1,1,1,1);

    }

    @Override
    public void act(float deltaTime){
        time -= deltaTime;
        if (time < 0){
            remove();
        }
        moveBy(0,deltaTime*speed*100);
        color.g = color.b -= (deltaTime * 0.5f);
        font.setColor(color);
    }

    @Override
    public void draw(Batch batch, float alpha){
        font.draw(batch, text, getX(), getY());
    }

}
