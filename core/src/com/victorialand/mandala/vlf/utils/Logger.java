package com.victorialand.mandala.vlf.utils;

import com.badlogic.gdx.Gdx;
import com.victorialand.mandala.vlf.GameConfig;
import com.victorialand.mandala.Main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Santiago on 08/10/2016.
 */
public class Logger{

    private ArrayList<String> lines;
    private FileWriter fileWriter;
    public int tabsNumber = 0;
    private int tabSize = 4;

    public Logger(String name){

        try{
            lines = new ArrayList<String>();
            if (GameConfig.writeExternalOutput){
                fileWriter = new FileWriter(GameConfig.externalPath+"output/"+name);
            }
            else{
                fileWriter = new FileWriter("output/"+name);
            }

        }
        catch (IOException e){
           // Main.debugger.set("Logger>_constructor",e.toString());
        }

    }

    public void setTabSize(int size){
        tabSize = size;
    }

    public void addLine(String line){
        String spaces = "";
        for (int i = 0; i < tabsNumber*tabSize; i++){
            spaces += " ";
        }
        lines.add(spaces+line+"\n");

    }

    public void addLine(String line, float value){

        lines.add(line+": " + value + "\n");

    }

    public void addLine(String line, String value){

        lines.add(line+": " + value + "\n");

    }

    public void logs(){

        try{
            for(int i = 0; i < lines.size(); i++){
                fileWriter.write(i + "." + lines.get(i));
            }
            fileWriter.flush();
            fileWriter.close();
        }
        catch (IOException e){
            Main.debugger.set("Logger>logs(): ",e.toString());
        }

    }

    public void logout(){

        if (GameConfig.log){
            logs();
        }
        Gdx.app.exit();

    }

}
