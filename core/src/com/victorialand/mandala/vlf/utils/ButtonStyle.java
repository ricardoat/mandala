package com.victorialand.mandala.vlf.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Created by Santiago on 02/08/2016.
 */
public class ButtonStyle{

    public TextButtonStyle textButtonStyle;
    public Drawable drawableUp;
    private Drawable drawableDown;
    private Drawable drawableChecked;
    private Color downFontColor;
    private Color checkedFontColor;
    private Color checkedOverFontColor;
    private Color disabledFontColor;
    public BitmapFont font;
    private Color fontColor;
    private Color overFontColor;


    public ButtonStyle(Drawable drawable, BitmapFont font){
        textButtonStyle = new TextButtonStyle(drawable,drawable,drawable,font);
        drawableUp      = drawable;
        drawableDown    = drawable;
        drawableChecked = drawable;
        this.font       = font;
    }

    public void setCheckedFontColor(Color color){
        textButtonStyle.checkedFontColor   = color;
        this.checkedFontColor              = color;
    }

    public void setCheckedOverFontColor(Color color){
        textButtonStyle.checkedOverFontColor   = color;
        this.checkedOverFontColor              = color;
    }

    public void setDisabledFontColor(Color color){
        textButtonStyle.disabledFontColor   = color;
        this.disabledFontColor              = color;
    }

    public void setDownFontColor(Color color){
        textButtonStyle.downFontColor   = color;
        this.downFontColor              = color;
    }

    public void setFont(BitmapFont font){
        textButtonStyle.font    = font;
        this.font               = font;
    }

    public void setFontColor(Color color){
        textButtonStyle.fontColor   = color;
        this.fontColor              = color;
    }

    public void setOverFontColor(Color color){
        textButtonStyle.disabledFontColor   = color;
        this.disabledFontColor              = color;
    }

    public void setDrawableUp(Drawable drawableUp){
        this.drawableUp = drawableUp;
        textButtonStyle = new TextButtonStyle(drawableUp,drawableDown,drawableChecked,font);
        reFill();
    }

    public void setDrawableDown(Drawable drawableDown){
        this.drawableDown         = drawableDown;
        textButtonStyle = new TextButtonStyle(drawableUp,drawableDown,drawableChecked,font);
        reFill();
    }

    public void setDrawableChecked(Drawable drawableChecked){
        this.drawableChecked         = drawableChecked;
        textButtonStyle = new TextButtonStyle(drawableUp,drawableDown,drawableChecked,font);
        reFill();
    }

    public void reFill(){
        textButtonStyle.checkedFontColor        = checkedFontColor;
        textButtonStyle.checkedOverFontColor    = checkedOverFontColor;
        textButtonStyle.disabledFontColor       = disabledFontColor;
        textButtonStyle.downFontColor           = downFontColor;
        textButtonStyle.fontColor               = fontColor;
        textButtonStyle.overFontColor           = overFontColor;
    }
}
