package com.victorialand.mandala.vlf;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.victorialand.mandala.vlf.configuration.Config;

/**
 * Created by Santiago on 28/07/2016.
 */
public class Background extends Actor {

    public float speed = 2;
    Sprite sprite;
    public int id;
    boolean move;


    public Background(Texture texture, int width, int height, boolean move){
        this.move = move;
        sprite = new Sprite(texture);
        setSize(width,height);
    }

    @Override
    public void draw(Batch batch, float alpha){
        batch.draw(sprite,getX(),getY(),getOriginX(),getOriginY(),getWidth(),getHeight(),getScaleX(),getScaleY(),getRotation());
    }

    @Override
    public void act(float deltaTime){
    }

}
