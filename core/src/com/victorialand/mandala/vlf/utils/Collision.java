package com.victorialand.mandala.vlf.utils;

/**
 * Created by Santiago on 30/08/2016.
 */


public class Collision {


    public int colliderAIndex, colliderBIndex;
    public float topDif, bottomDif, leftDif, rightDif;
    public boolean collision;
    public CollisionDirection direction;

    public Collision(){
    }

    public Collision(int colliderAIndex, int colliderBIndex, Collider colliderA, Collider colliderB){

        this.colliderAIndex = colliderAIndex;
        this.colliderBIndex = colliderBIndex;

        topDif = Math.abs(colliderA.getBottomLimit()-colliderB.getTopLimit());
        bottomDif = Math.abs(colliderA.getTopLimit()-colliderB.getBottomLimit());
        leftDif = Math.abs(colliderA.getRightLimit()-colliderB.getLeftLimit());
        rightDif = Math.abs(colliderA.getLeftLimit()-colliderB.getRightLimit());

        if (topDif < bottomDif && topDif < leftDif && topDif < rightDif){
            direction = CollisionDirection.TOP;
        }
        else if (bottomDif < leftDif && bottomDif < rightDif){
            direction = CollisionDirection.BOTTOM;
        }
        else if(leftDif < rightDif){
            direction = CollisionDirection.LEFT;
        }
        else{
            direction = CollisionDirection.RIGHT;
        }
        collision = true;
    }

}
