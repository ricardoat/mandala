package com.victorialand.mandala.vlf.utils;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.victorialand.mandala.Main;

/**
 * Created by root on 4/27/17.
 */

public class ImageRect extends Actor {

    Sprite image;
    String id;

    public ImageRect(Texture image, int x, int y, int width, int height, String id){

        this.image = new Sprite(image);
        setSize(width,height);
        setPosition(x,y);
        this.id = id;

    }

    @Override
    public void draw(Batch batch, float alpha){

        batch.draw(image, getX(),getY(),getOriginX(),getOriginY(),getWidth(),getHeight(),getScaleX(),getScaleY(),getRotation());
    }


}
