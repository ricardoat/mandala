package com.victorialand.mandala.vlf.utils;

/**
 * Created by Santiago on 30/08/2016.
 */
public enum CollisionDirection {

    TOP,
    BOTTOM,
    LEFT,
    RIGHT

}
