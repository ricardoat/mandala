package com.victorialand.mandala.vlf;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.victorialand.mandala.Assets;
import com.victorialand.mandala.LoadingScreen;
import com.victorialand.mandala.vlf.configuration.Config;
import com.victorialand.mandala.vlf.utils.ImageRect;

/**
 * Created by Santiago on 02/08/2016.
 */
public class FirstScreen extends ScreenAdapter {

    @Override
    public void show(){
        Config.game.setScreen(new LoadingScreen());
    }
}
