package com.victorialand.mandala.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.victorialand.mandala.Assets;
import com.victorialand.mandala.GameScreen;
import com.victorialand.mandala.vlf.configuration.Config;
import com.victorialand.mandala.vlf.utils.VL;

import java.util.ArrayList;

/**
 * Created by ricardo on 20/07/17.
 */

public class Mandala extends Group {

    int centerX;
    int centerY;
    public Circulo[] circulos;

    public Mandala(int centerX, int centerY) {

        this.centerX = centerX;
        this.centerY = centerY;

        circulos = new Circulo[] {
                new Circulo(Assets.imgCircles[0],Config.sizes.circlesSize[0].width/2,this),
                new Circulo(Assets.imgCircles[1],Config.sizes.circlesSize[1].width/2,this),
                new Circulo(Assets.imgCircles[2],Config.sizes.circlesSize[2].width/2,this),
                new Circulo(Assets.imgCircles[3],Config.sizes.circlesSize[3].width/2,this),
                new Circulo(Assets.imgCircles[4],Config.sizes.circlesSize[4].width/2,this),
                new Circulo(Assets.imgCircles[5],Config.sizes.circlesSize[5].width/2,this),
                new Circulo(Assets.imgCircles[6],Config.sizes.circlesSize[6].width/2,this),
        };

        //setPosition(centerX-Config.sizes.circlesSize[6].width/2,centerY-Config.sizes.circlesSize[6].height/2);

        for (int i=0; i<7; i++){
            addActor(circulos[i]);
        }
    }

    public void rotate(int rotationTime, int degrees, int screenX, int screenY){

        int catX = screenX - centerX;
        int catY = screenY - centerY;
        int hip = (int) Math.sqrt(Math.pow(catX,2) + Math.pow(catY,2));

        if (hip < Config.sizes.circlesSize[6].width/2){
            circulos[0].rotate(rotationTime,degrees);

            if (hip > Config.sizes.circlesSize[0].width/2){
                circulos[1].rotate(rotationTime,degrees);

                if (hip > Config.sizes.circlesSize[1].width/2){
                    circulos[2].rotate(rotationTime,degrees);

                    if (hip > Config.sizes.circlesSize[2].width/2) {
                        circulos[3].rotate(rotationTime,degrees);

                        if (hip > Config.sizes.circlesSize[3].width/2){
                            circulos[4].rotate(rotationTime,degrees);

                            if (hip > Config.sizes.circlesSize[4].width/2){
                                circulos[5].rotate(rotationTime,degrees);

                                if (hip > Config.sizes.circlesSize[5].width/2){
                                    circulos[6].rotate(rotationTime,degrees);
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}
