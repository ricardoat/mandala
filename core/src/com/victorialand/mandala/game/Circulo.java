package com.victorialand.mandala.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.victorialand.mandala.Main;
import com.victorialand.mandala.vlf.configuration.Config;

/**
 * Created by ricardo on 27/07/17.
 */

public class Circulo extends Actor {

    Sprite img;
    float rotationTimeLeft;
    int currentRotationDegrees;
    int currentRotationTarget;
    float currentRotationTime;
    public int ratio;
    Mandala parentMandala;

    public Circulo(Texture texture, int ratio, Mandala parentMandala){

        this.parentMandala = parentMandala;
        img = new Sprite(texture);
        this.ratio = ratio;
        setPosition(parentMandala.centerX-ratio,parentMandala.centerY-ratio);
        setOrigin(ratio,ratio);
        setSize(ratio*2,ratio*2);

    }

    public void rotate(float rotationTime, int degrees){

        rotationTimeLeft = currentRotationTime = rotationTime;
        currentRotationDegrees = degrees;
        currentRotationTarget = (int) getRotation() + degrees;

    }

    @Override
    public void act(float deltaTime) {
        if (rotationTimeLeft > 0){
            rotateBy(currentRotationDegrees*deltaTime/currentRotationTime);
            rotationTimeLeft-=deltaTime;
            if (rotationTimeLeft <= 0){
                setRotation(currentRotationTarget);
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(),
                getOriginX(), getOriginY(),
                getWidth(), getHeight(),
                getScaleX(), getScaleY(),
                getRotation());
    }


}
